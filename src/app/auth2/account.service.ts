import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, shareReplay, tap } from 'rxjs/operators';
import { Authority } from './auth-shared/constans/authority.constants';
import { ICredentials } from './auth-shared/models/credentials';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCahche?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router

  ) { }

    create(account: ICredentials): Observable <ICredentials>{
      return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, Credential);
    }

  identity(force?: boolean): Observable<Account | null>{
    if(!this.accountCahche || force || !this.isAutenticated()){
        this.accountCahche = this.fetch().pipe(catchError(()=> {
          return of(null);
        }),tap((account:Account | null)=> {
            this.autenticate(account);
            if(account){
              this.router.navigate(['/dashboard'])
            }
        }),shareReplay());
    }
    return this.accountCahche;
  }
  autenticate(account: Account | null): void{
    this.userIdentity = account;

  }

  isAutenticated(): boolean{
    return this.userIdentity !==null;
  }
  getUsername(): string{
    return this.userIdentity.firstName + " " + this.userIdentity.lastName;

  }
  getAuthenticationState():Observable< Account | null>{
    return this.authenticationState.asObservable();
  }

  hasAnyAuthority(authorities: string[] | string): boolean{
    if(!this.userIdentity || !this.userIdentity.authorities){
      return false;
    }
    if(!Array.isArray(authorities)){
      authorities = [authorities]
    }
    return this.userIdentity.authorities.some((authority: string)=> authorities.includes(authority))
  }

  private fetch(): Observable<Account>{
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }
}
