import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';import { AuthService } from '../auth.service';
import { ICredentials , Credentials } from '../auth-shared/models/credentials';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    userName:[''],
    password:[''],
    rememberMe:[''],
  });
  constructor(private fb:FormBuilder,
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit(): void {
  }

  login(): void {
    console.warn('datos', this.loginForm.value);
    const credentials: ICredentials = new Credentials();
        credentials.username = this.loginForm.value.userName;
        credentials.password = this.loginForm.value.password;
        credentials.rememberMe = this.loginForm.value.rememberMe;
    
        this.loginService.login(credentials)
        .subscribe((res: any) => {
          console.warn('DATA LOGIN CONTROLER - ok Login', res)
          this.router.navigate(['/dashboard'])
        }, (error: any) => {
          if (error.status === 400){
            console.warn('Usuario o contraseña incorectos');
          }
        })
  }
}
