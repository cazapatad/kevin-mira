import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth2/login/login.component';
import { AccesDeniedComponent } from './auth2/acces-denied/acces-denied.component';
import { Authority } from './auth2/auth-shared/constans/authority.constants';
import { UserRouteAccessService } from './auth2/user-route-access.service';


const routes: Routes = [
  {
    path: 'dashboard',
    data:{
      authorities:[
        Authority.ADMIN, Authority.USER
      ]
    },
    canActivate:[
      UserRouteAccessService
    ],
    loadChildren: () => import('./dashboard/dashboard.module')
    .then(m => m.DashboardModule)
  },
  {
    path:'auth',
    component:LoginComponent
  },
  {
    path: 'acces-deneid',
    component: AccesDeniedComponent
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
