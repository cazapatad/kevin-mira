import { IBike } from '../bikes/bike.interface';

export interface ISale {
    id?: number,
    date?: Date,
    clientId?: number,
    clientName?: string,
    bikeId?: number,
    bikeSerial?: string,
    client?: any,
    bike?: IBike
}
