import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IClient } from './clients';
import { createRequestOption } from 'src/app/utils/request_utils';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IClient[]> {
    const params = createRequestOption(req);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients`,{ params: params })
    .pipe(map(res => {
      return res;
    }));
  }

  public findClientByDocument(req?: any): Observable<IClient[]> {
    const params = createRequestOption(req);
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/clients/find-by-document`,{ params: params })
    .pipe(map(res => {
      return res;
    }));
  }

}
