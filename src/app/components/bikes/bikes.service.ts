import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBike } from './bike.interface';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { createRequestOption } from 'src/app/utils/request_utils';
import { IBikeWithClient } from 'src/app/shared/models/bike-with-client';
@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor(private http: HttpClient) { }

  public query(req?: any): Observable<IBike[]> {

    const params = createRequestOption(req);
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bikes`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public saveBike(bike: IBikeWithClient): Observable<IBike> {
    return this.http.post<IBike>(`${environment.END_POINT}/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }

  public getBikeById(id: string): Observable<IBike> {
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
  
  public updateBike(bike: IBike): Observable<IBike> {
    return this.http.put<IBike>(`${environment.END_POINT}/api/bikes`, bike)
    .pipe(map(res => {
      return res;
    }));
  }

  /**
   * This method is for get all bikes by serial implemented Query String
   * @param id 
   */
  public getBikeBySerial(serial: string): Observable<IBike> {
    let params = new HttpParams();
    params = params.set('serial',serial);
    return this.http.get<IBike>(`${environment.END_POINT}/api/bikes/find-bike-by-serial`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }
  

  public deleteItem(id: string): Observable<IBike> {
    return this.http.delete<IBike>(`${environment.END_POINT}/api/bikes/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }
  

}
